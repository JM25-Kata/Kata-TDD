//
// Created by Meritis Local on 08/10/2019.
//

#include "core-example.h"
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include <map>
#include <algorithm>

bool functionExample(bool param) {
    return param;
}

std::map<int, std::string > numToSigle = {
        {10, "X"},
        {5, "V"},
        {4, "IV"},
        {1, "I"}
};

std::string divide(int& num, int divider) {
    std::stringstream ss;
    while (num >= divider) {
        ss << numToSigle.at(divider);
        num -= divider;
    }
    return ss.str();
}

std::string convert(int num) {
    std::stringstream ss;

    std::for_each(numToSigle.rbegin(), numToSigle.rend(),
                  [&ss, &num](auto element){
                      ss << divide(num, element.first);
                  });
    return ss.str();
}