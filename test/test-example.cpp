//
// Created by Meritis Local on 08/10/2019.
//

#include <string>
#include "gtest/gtest.h"
#include "../src/core-example.h"

TEST(TestSuiteName, TestCaseName)
{
    EXPECT_EQ(convert(1), "I");
    EXPECT_EQ(convert(2), "II");
    EXPECT_EQ(convert(10), "X");
    EXPECT_EQ(convert(11), "XI");
    EXPECT_EQ(convert(12), "XII");
    EXPECT_EQ(convert(20), "XX");
    EXPECT_EQ(convert(22), "XXII");
    EXPECT_EQ(convert(5), "V");
    EXPECT_EQ(convert(15), "XV");
    EXPECT_EQ(convert(16), "XVI");
    EXPECT_EQ(convert(4), "IV");
}