# Convertisseur de nombres romains par le TDD

Le but est de déveloper un convertisseur de nombres romains en utilisant la méthode de développement TDD.
Les différentes étapes du développent par TDD seront identifiées par des commits.
Note : Finalement je n'ai pas pensé à le faire pendant l'exercise on a donc les grandes étapes de tests mais pas les étapes TDD arrivant à ces résultats.

# Stratégie

La stratégie du TDD est:
* Repondre le plus simplement possible à une spécification. 
* Augmenter la complexité uniquement pour répondre à une nouvelle spécification
* Refactorer uniquement pour simplifier

## Sélection des spécifications pour les nombres romains
Je ne vais pas décrire les spécifications et partir du principe que vous les connaissez (au moins I, IV, V, VI, IX, X, XI).
Le plus simple est évidemment "I". Les plus complexe semblent être les soustractions "IV" et "IX". Nous allons donc itérer le développement de I à IX.
Après I et ayant exclus IV, on serait tenter de choisir V. Cependant X peut être un meilleurs choix.